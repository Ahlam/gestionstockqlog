Configurer la base de données:

Pré-requis

​	MySQL server

​	Mac XAMPP:https://www.apachefriends.org/index.html



​	1.Créer une base de données （magasin)

Faire correspondre le "magasin" dans l'url au nom de la base de données

```
<property name="javax.persistence.jdbc.url" value="jdbc:mysql://127.0.0.1/magasin?serverTimezone=UTC"/>
```

​	2.Modifier le nom d'utilisateur et le mot de passe dans le fichier gestionstockqlog/gestionstockqlog/src/META-INF/persistence.xml

```
<property name="javax.persistence.jdbc.user" value="root"/>
<property name="javax.persistence.jdbc.password" value=""/>
```

​	3.Enlever la commentaire

```
<property name="hibernate.hbm2ddl.auto" value="create"/>
```

 dans le fichier gestionstockqlog/gestionstockqlog/src/META-INF/persistence.xml 

​	4.Lancer InitilizeDataBase.java dans le package start. Il est utilisé pour ajouter des données.

​	

Lancer le programme:

​	1.Mis en commentaire 

```
<property name="hibernate.hbm2ddl.auto" value="create"/>
```



​	2 .Lancer Main.java dans la package start.



Par default on a :

​	Id pour le responsable : 6  

​	Id pour vendeur : 7 ou 8

​	Le mot de passe : root