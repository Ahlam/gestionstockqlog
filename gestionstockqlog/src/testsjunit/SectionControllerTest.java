/**
 * 
 */
package testsjunit;

import enums.RESULT;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SectionControllerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * Test method for {@link controller.SectionController#createSection(java.lang.String)}.
	 */
	@Test
	void testCreateSection() {
		enums.RESULT result = controller.SectionController.createSection("");
		assertEquals(RESULT.EMPTY_ERROR, result);
		
		//DUPLICATE_ERROR
		
		//un test come �a ne passera qu'une seule fois tant qu'on a pas mock
//		RESULT result1 = controller.SectionController.createSection("Tennis");
//		System.out.println("Result :"+result1);
//		assertEquals(RESULT.SUCCESS, result1);	
	}

	/**
	 * Test method for {@link controller.SectionController#updateSection(int, java.lang.String, java.lang.String)}.
	 */
	@Test
	void testUpdateSection() {
		RESULT result = controller.SectionController.updateSection(14, "", "Toto");
		assertEquals(RESULT.EMPTY_ERROR, result);
		RESULT result1 = controller.SectionController.updateSection(14, "Tennis", "Toto Gernaos");
		
	}

}
