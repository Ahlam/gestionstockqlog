package testsjunit;

import enums.RESULT;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author ahlam
 *
 */
class ItemControllerTest {
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		//itemDao = mock(dao.ItemDao.class);
		//doNothing().when(itemDao).createItem("Raquette", "Tennis", "description", 122, 13);
//		PowerMock.mockStatic(dao.ItemDao.class);
//		doNothing().when(dao.ItemDao.createItem("Raquette", "Tennis", "description", 122, 13));
		// PowerMock.same(dao.ItemDao.createItem("Raquette", "Tennis", "description", 122, 13))
		//mockStatic(dao.ItemDao.class);
        //BDDMockito. (dao.ItemDao.createItem("Raquette", "Tennis", "description", 122, 13));
		
		//when(item.getDescription()).thenReturn();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		
	}
	
	/**
	 * Test method for {@link controller.ItemController#createItem(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 */
	@Test
	void testCreateItem() {
		RESULT result = controller.ItemController.createItem("", "", "", "", 0);
		assertEquals(RESULT.EMPTY_ERROR, result);
		RESULT result2 = controller.ItemController.createItem("", "Tennis", "description", "122", 0);
		assertEquals(RESULT.EMPTY_ERROR, result2);
		RESULT result3 = controller.ItemController.createItem("Raquette", "Tennis", "description", "12d2", 0);
		assertEquals(RESULT.NUMBER_FORMAT_EXCEPTION, result3);
		RESULT result4 = controller.ItemController.createItem("Raquette", "Tennis", "description", "122", 13);
		assertEquals(RESULT.SUCCESS, result4);
	}

	/**
	 * Test method for {@link controller.ItemController#incrementerArcticleStock(int, java.lang.String, javax.swing.JFrame)}.
	 */
	@Test
	void testIncrementerArcticleStock() {
		RESULT result = controller.ItemController.incrementItemStock(-1, "115");
		assertEquals(RESULT.NOT_SELECTED_ERROR, result);
		RESULT result1 = controller.ItemController.incrementItemStock(5, "115");
		assertEquals(RESULT.SUCCESS, result1);
		RESULT result2 = controller.ItemController.incrementItemStock(5, "1k15");
		assertEquals(RESULT.NUMBER_FORMAT_EXCEPTION, result2);
	}

	/**
	 * Test method for {@link controller.ItemController#decrementerArcticleStock(int, java.lang.String, javax.swing.JFrame)}.
	 */
	@Test
	void testDecrementerItemStock() {
		RESULT result = controller.ItemController.decrementItemStock(-1, "315");
		assertEquals(RESULT.NOT_SELECTED_ERROR, result);
		RESULT result1 = controller.ItemController.decrementItemStock(4, "315");
		assertEquals(RESULT.SUCCESS, result1);
		RESULT result2 = controller.ItemController.decrementItemStock(4, "31sd5");
		assertEquals(RESULT.NUMBER_FORMAT_EXCEPTION, result2);
	}

	/**
	 * Test method for {@link controller.ItemController#updateArticle(int, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	void testUpdateItem() {
		RESULT result = controller.ItemController.updateItem(5, "Ballon", "type", "54");
		assertEquals(RESULT.SUCCESS, result);
		RESULT result1 = controller.ItemController.updateItem(5, "Ballon", "type", "5fd4");
		assertEquals(RESULT.NUMBER_FORMAT_EXCEPTION, result1);
	}
}
