package controller;

import enums.FUNCTION;
import enums.RESULT;
import dao.SectionDao;
import dao.UserDao;
import model.Section;

public class UserController {

	public static RESULT createUser(String lastName,String firstName,String nomSection,String passwordString) {
		
		if (lastName.isEmpty()||firstName.isEmpty()||passwordString.isEmpty()) {
			return RESULT.EMPTY_ERROR;
			
		}else {
			if (UserDao.getUser(firstName, lastName)!=null) {
				return RESULT.DUPLICATE_ERROR;
			}
		}
		Section section = SectionDao.getSection(nomSection);
		UserDao.createUser(lastName, firstName, section, passwordString);
		return RESULT.SUCCESS;
		
	}
	public static RESULT updateUser(int id,String lastName,String firstName,String section,String password,String function) {
		
		if (lastName.isEmpty() || firstName.isEmpty() || password.isEmpty()) {
			return RESULT.EMPTY_ERROR;
		}

			if (function.equals(FUNCTION.MANAGER.toString())) {
				if (!section.equals("Tout")) {
					return RESULT.ALLOCATION_ERROR;
				} else {
					UserDao.updateUser(id, lastName, firstName, null, password);
					return RESULT.SUCCESS;
				}
			}

			Section section2 = SectionDao.getSection(section);
			if (section2 == null) {
				return RESULT.NO_RESULT_FOUND_ERROR;
			} else {
				if (section2.getSeller() != null) {
					UserDao.updateUser(section2.getSeller().getId(),
							section2.getSeller().getLastName(), section2.getSeller().getFirtsName(),
							null, section2.getSeller().getPassword());
				}
				UserDao.updateUser(id, lastName, firstName, section2, password);
			}



		return RESULT.SUCCESS;
	}
}
