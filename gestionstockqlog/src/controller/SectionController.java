package controller;

import dao.SectionDao;
import dao.UserDao;
import enums.FUNCTION;
import enums.RESULT;
import model.Section;
import model.User;

import java.util.List;

public class SectionController {

	/**
	 * 
	 * @return the list of the store's sections
	 */
	public static List<Section> getAllSections() {
		List<Section> sections = SectionDao.getSectionList();
		return sections;
	}

	/**
	 * returns a section by its name
	 * @param sectionName
	 * @return a section
	 */
	public static Section getSection(String sectionName) {
		Section section = SectionDao.getSection(sectionName);
		return section;
	}

	/**
	 * creates a section using the section's name
	 * @param sectionName
	 * @return EMPTY_ERROR or DUPICATE_ERROR
	 */
	public static RESULT createSection(String sectionName) {
		if (sectionName.isEmpty()) {
			return RESULT.EMPTY_ERROR;
		} else {
			if (SectionDao.getSection(sectionName) != null) {
				return RESULT.DUPLICATE_ERROR;
			}
		}
		SectionDao.createSection(sectionName);
		return RESULT.SUCCESS;
	}

	/**
	 * 
	 * @param id the section's identifier
	 * @param sectionName
	 * @param managerName
	 * @return EMPTY_ERROR or MANAGER_NAME_LENGTH_ERROR or NO_RESULT_FOUND_ERROR or ALLOCATION_ERROR
	 */
	public static RESULT updateSection(int id, String sectionName, String managerName) {
		if (sectionName.isEmpty()) {
			return RESULT.EMPTY_ERROR;
		} else {
			String[] splited = managerName.split(" ");
			if (splited.length != 2) {
				return RESULT.MANAGER_NAME_LENGTH_ERROR;
			} else {
				User user = UserDao.getUser(splited[0], splited[1]);
				if (user == null) {
					return RESULT.NO_RESULT_FOUND_ERROR;
				} else {
					if (user.getFunction() == FUNCTION.MANAGER) {
						return RESULT.ALLOCATION_ERROR;
					} else {
						SectionDao.updateSection(id, sectionName, user);
					}
				}
			}
		}
		return RESULT.SUCCESS;
	}
}
