package controller;

import enums.RESULT;
import dao.ItemDao;
import model.Item;

public class ItemController {
	/**
	 * 
	 * @param itemName
	 * @param type
	 * @param description
	 * @param stockString
	 * @param sectionId
	 * @return EMPTY_ERROR or NUMBER_FORMAT_EXCEPTION or SUCCESS
	 */
	public static RESULT createItem(String itemName, String type, String description, String stockString, int sectionId) {
		if (itemName.isEmpty()) {
			return RESULT.EMPTY_ERROR;
		}
		try {
			int stock = Integer.parseInt(stockString);
			ItemDao.createItem(itemName, type, description, stock, sectionId);
		} catch (NumberFormatException e2) {
			return RESULT.NUMBER_FORMAT_EXCEPTION;
		}
		return RESULT.SUCCESS;
	}

	/**
	 * 
	 * @param id the item id
	 * @param stockString number of items to add to the stock
	 * @return NOT_SELECTED_ERROR or SUCCESS or NUMBER_FORMAT_EXCEPTION
	 */
	public static RESULT incrementItemStock(int id, String stockString) {
		if (id == -1) {
			return RESULT.NOT_SELECTED_ERROR;
		} else {
			Item item = ItemDao.getItem(id);
			if (stockString.isEmpty()) {
				ItemDao.updateItem(id, item.getStock() + 1);
				return RESULT.SUCCESS;
			} else {
				try {
					int inputNb = Integer.parseInt(stockString);
					ItemDao.updateItem(id, item.getStock() + inputNb);
					return RESULT.SUCCESS;
				} catch (NumberFormatException e2) {
					return RESULT.NUMBER_FORMAT_EXCEPTION;
				}
			}
		}
	}

	/**
	 * 
	 * @param id the item id
	 * @param stockString number of items to remove from the stock
	 * @return NOT_SELECTED_ERROR or SUCCESS or NUMBER_FORMAT_EXCEPTION
	 */
	public static RESULT decrementItemStock(int id, String stockString) {
		if (id == -1) {
			return RESULT.NOT_SELECTED_ERROR;
		} else {
			Item item = ItemDao.getItem(id);
			if (stockString.isEmpty()) {
				ItemDao.updateItem(id, item.getStock() - 1);
				return RESULT.SUCCESS;
			} else {
				try {
					int inputNb = Integer.parseInt(stockString);
					int stock = item.getStock() - inputNb <= 0 ? 0 : item.getStock() - inputNb;
					ItemDao.updateItem(id, stock);
					return RESULT.SUCCESS;
				} catch (NumberFormatException e2) {
					return RESULT.NUMBER_FORMAT_EXCEPTION;
				}
			}
		}
	}

	/**
	 * updates an item's information
	 * @param id the item id
	 * @param itemName
	 * @param type the item type
	 * @param stockString number of items in the stock
	 * @return SUCCESS or NUMBER_FORMAT_EXCEPTION
	 */
	public static RESULT updateItem(int id, String itemName, String type, String stockString) {
		try {
			int stock = Integer.parseInt(stockString);
			ItemDao.updateItem(id, itemName, type, stock);
			return RESULT.SUCCESS;	
		} catch (NumberFormatException e2) {
			return RESULT.NUMBER_FORMAT_EXCEPTION;			
		}	
	}

	/**
	 * updates the item's description
	 * @param id the item's id
	 * @param description the item's description
	 */
	public static void updateItemDescription(int id, String description) {
		ItemDao.updateItem(id, description);
	}

	/**
	 * returns the item's description
	 * @param id the item's identifier
	 * @return the item's description
	 */
	public static String getItemDescription(int id) {
		Item item = ItemDao.getItem(id);
		return item.getDescription();
	}

	/**
	 * removes an item
	 * @param id the item's identifier
	 */
	public static void removeItem(int id) {
		ItemDao.removeItem(id);
	}
}
