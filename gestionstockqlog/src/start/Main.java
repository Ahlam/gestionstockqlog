package start;

import view.LoginWindow;

import java.awt.*;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {

				LoginWindow window = new LoginWindow();
				window.getFrame().setVisible(true);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		});
	}
}
