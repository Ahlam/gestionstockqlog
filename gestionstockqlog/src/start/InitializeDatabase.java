package start;

import enums.FUNCTION;
import model.Item;
import model.Section;
import model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class InitializeDatabase {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");
		EntityManager em = entityManagerFactory.createEntityManager();
		Item a1 = new Item("A13","shoes","Inspirée par les flux d'énergie présents sur Terre, comme les coulées de lave et le rythme des vagues.");
		Item a2 = new Item("A12","T-shit","La veste à capuche Nike Sportswear Windrunner associe isolation légère et design réversible pour un look polyvalent et fonctionnel. ");
		Item a3 = new Item("A120","T-shit","pas mal");
		User u1 = new User("root","root",FUNCTION.MANAGER);
		User v1 = new User("vendeur1","ven",FUNCTION.SELLER);
		User v2 = new User("vendeur2","ven",FUNCTION.SELLER);
		
		u1.setPassword("root");
		v1.setPassword("123");
		v2.setPassword("123");
		Section R1 = new Section();
		Section R2 = new Section();
		R1.setSectionName("Basketsball");
		R2.setSectionName("Football");
		v1.setSection(R1);
		v2.setSection(R2);
		
		a1.setSection(R1);
		a2.setSection(R1);
		a3.setSection(R2);
		R1.setSeller(v1);
		R1.getArticles().add(a1);
		R1.getArticles().add(a2);
		R2.setSeller(v2);
		R2.getArticles().add(a3);
		
		em.getTransaction().begin();
		em.persist(R1);
		em.persist(R2);
		
		em.persist(u1);
		em.persist(v1);
		em.persist(v2);
		
		
		em.getTransaction().commit();
		
		
	}

}
