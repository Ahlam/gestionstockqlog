package enums;
/**
 * <b> RESULT is the enumeration of all the values that the function of controllers may return </b>
 * 
 * @author Yang YANG 
 *
 */
public enum RESULT {
	SUCCESS,EMPTY_ERROR, DUPLICATE_ERROR,NUMBER_FORMAT_EXCEPTION,NOT_SELECTED_ERROR,MANAGER_NAME_LENGTH_ERROR,ALLOCATION_ERROR,
	NO_RESULT_FOUND_ERROR
}
