package enums;
/**
 * <b> FUNCTION is the enumeration representing  the function of the user </b>
 * 
 * @author Yang YANG 
 *
 */
public enum FUNCTION {
	SELLER,
	MANAGER
}
