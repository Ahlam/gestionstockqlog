package model;

import enums.FUNCTION;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * <b> User is the class representing a user in the shop. </b>
 * 
 * Manager and seller are both user of the application.
 * 
 * @author Yang YANG
 * 
 */
@Entity
public class User {
	/**
	 * The id of the user
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	/**
	 * The last name of the user
	 */
	private String lastName;
	/**
	 * The first name of the user
	 */
	private String firstName;
	/**
	 * The password of the user
	 */
	private String password;
	
	/**
	 * The function of the user
	 */
	@Enumerated(EnumType.STRING)
	private FUNCTION function;
	
	/**
	 * The function of the user
	 */
	@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	private Section section;
	/**
	 * A default constructor 
	 */
	public User() {

	}
	/**
	 * A parameterized constructor 
	 * Initializes an object User's attributes by the attributes passed as parameters of the method
	 * @param lastName
	 * @param firstName
	 * @param fUNCTION
	 */
	public User(String lastName, String firstName, FUNCTION fUNCTION) {

		this.lastName = lastName;
		this.firstName = firstName;
		this.function = fUNCTION;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirtsName() {
		return firstName;
	}

	public void setFirtsName(String firstName) {
		this.firstName = firstName;
	}

	public FUNCTION getFunction() {
		return function;
	}

	public void setFunction(FUNCTION function) {
		this.function = function;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
