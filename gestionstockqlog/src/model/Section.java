package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
/**
 * <b> Section is the class representing  a section in the shop. </b>
 * 
 * @author Yang YANG 
 * 
 */
@Entity
public class Section {
	
	/**
	 * The id of the section
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/**
	 * The name of the section
	 */
	private String sectionName;
	
	/**
	 * The seller who is responsible for the section
	 */
	@OneToOne(mappedBy="section")
	private User seller;
	/**
	 * Items in the section
	 */
	@OneToMany(mappedBy="section" ,cascade = CascadeType.ALL)
	private List<Item> articles=new ArrayList<Item>();
	
	/**
	 * A default constructor 
	 */
	public Section() {
		//Hibernate :  All persistent classes must have a default constructor
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getSeller() {
		return seller;
	}
	public void setSeller(User seller) {
		this.seller = seller;
	}
	public void setArticles(List<Item> articles) {
		this.articles = articles;
	}
	public List<Item> getArticles() {
		return articles;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	
	
}
