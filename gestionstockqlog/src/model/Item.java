package model;

import javax.persistence.*;

/**
 * <b> Item is the class representing  an item of the shop </b>
 * <p>
 * 	An item belong to only one section. 
 * 	An item is characterized by the following information :
 * <ul>
 * <li> An identifier </li>
 * <li> An itemName </li>
 * <li> A type </li>
 * <li> A description </li>
 * <li> A department </li>
 * <li> A stock </li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG 
 *
 */
@Entity
public class Item {
	
	/**
	 * The id of the item
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/**
	 * The name of the item
	 */
	private String itemName;
	
	/**
	 * The type of the item
	 */
	private String type;
	
	/**
	 * The stock of the item
	 */
	private int stock;
	
	/**
	 * The description of the item
	 */
	private String description;
	
	/**
	 * The section which the item belongs
	 */
	@ManyToOne
	private Section section;
	
	/**
	 * A default constructor 
	 */
	public Item() {
		
	}
	
	/**
	 * A parameterized constructor 
	 * Initializes an object Item's attributes by the attributes passed as parameters of the method
	 * @param itemName
	 * @param type
	 * @param description
	 */
	public Item(String itemName, String type, String description) {
		super();
		this.itemName = itemName;
		this.type = type;
		this.stock = 0;
		this.description = description;
	}
	
	/**
	 * A parameterized constructor 
	 * Initializes an object Item's attributes by the attributes passed as parameters of the method
	 * @param itemName
	 * @param type
	 * @param description
	 * @param stock
	 */
	public Item(String itemName, String type, String description,int stock) {
		super();
		this.itemName = itemName;
		this.type = type;
		this.stock = stock;
		this.description = description;
	}
	
	/**
	 * Gets the attribute department of the item
	 * @return the id of the employee
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Modifies the attribute Item of the object
	 * 
	 * @param  id
	 * 		 id of the Item
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the attribute itemName of the item
	 * @return the itemName of the item
	 */
	public String getitemName() {
		return itemName;
	}
	
	/**
	 * Modifies the attribute Item of the object
	 * 
	 * @param  itemName
	 * 		 itemName of the Item
	 */
	public void setitemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * Gets the attribute type of the item
	 * @return the type of the item
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Modifies the attribute type of the object
	 * 
	 * @param  type
	 * 		 type of the Item
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the attribute stock of the item
	 * @return the stock of the item
	 */
	public int getStock() {
		return stock;
	}
	
	/**
	 * Modifies the attribute stock of the object
	 * 
	 * @param  stock
	 * 		 stock of the Item
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	/**
	 * Gets the attribute description of the item
	 * @return the description of the item
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Modifies the attribute description of the item
	 * 
	 * @param  description
	 * 		 description of the Item
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the attribute section of the item
	 * @return the section of the item
	 */
	public Section getSection() {
		return section;
	}
	
	/**
	 * Modifies the attribute section of the item
	 * 
	 * @param  section
	 * 		 section of the Item
	 */
	public void setSection(Section section) {
		this.section = section;
	}
	
}
