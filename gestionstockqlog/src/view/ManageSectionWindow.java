package view;

import controller.ItemController;
import dao.ItemDao;
import enums.RESULT;
import model.Item;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * ManageSectionWindow is the class representing the window that opens when a
 * user(seller or manager) manage its section.
 * </p>
 *
 * <p>
 * A ManageSectionWindow is characterized by the following information :
 * <ul>
 * <li>A ManageSectionWindow that the EditDescriptionWindow will return back
 * to.</li>
 * <li>A JFrame contains all other required components.</li>
 *
 * <li>A table of items.</li>
 * <li>A DefaultTableModel of itemsTable.</li>
 * <li>The seller home page.</li>
 * <li>The manager home page.</li>
 * <li>The section id that the user is working on .</li>
 * <li>The window for editing description of item .</li>
 * <li>The window for creating an item .</li>
 * <li>A integer that allows to track item selected by user from table.</li>
 * </ul>
 * </p>
 *
 * @author Yang YANG
 */
public class ManageSectionWindow {
    /**
     * A JFrame contains all other required components in this window.
     */
    private JFrame frame;

    /**
     * A table of items.
     */
    private JTable itemsTable;

    /**
     * A DefaultTableModel of itemsTable.
     */
    private DefaultTableModel table_model;

    /**
     * The seller home page.
     */
    private SellerMainWindow sellerHomePage;

    /**
     * The manager home page.
     */
    private ManagerMainWindow managerHomePage;

    /**
     * The section id that the user is working on .
     */
    private int sectionId;

    /**
     * The window for editing description of item .
     */
    private EditDescriptionWindow editDescriptionWindow;

    /**
     * The window for creating an item .
     */
    private CreateAnItemWindow createAnItemWindow;

    /**
     * A integer that allows to track item selected by user from table.
     */
    private int selectedItemId = -1;
    /**
     * A message when found no selected a item or article.
     */
    private String notSelectedErrorMessage = "Faut d'abord sélectionner une ligne de données dans le tableau";
    /**
     * A title for alert window.
     */
    private String alertTitle = "Alert";
    /**
     * A title for success window.
     */
    private String successTitle = "Succès";

    /**
     * A parameterized constructor.
     *
     * @param sellerHomePage An object of the class SellerMainWindow
     * @param sectionId      The sectionId that user want manage
     */
    public ManageSectionWindow(SellerMainWindow sellerHomePage, int sectionId) {
        this.sellerHomePage = sellerHomePage;
        this.sectionId = sectionId;
        initialize();

    }

    /**
     * A parameterized constructor.
     *
     * @param managerHomePage An object of the class ManagerMainWindow
     * @param sectionId       The sectionId that user want manage
     */
    public ManageSectionWindow(ManagerMainWindow managerHomePage, int sectionId) {
        this.managerHomePage = managerHomePage;
        this.sectionId = sectionId;
        initialize();

    }

    /**
     * Gets JFrame.
     *
     * @return A JFrame contains all other required components.
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 607, 415);
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JPanel mainPanel = new JPanel();
        JPanel buttonAndTablePanel = new JPanel();
        buttonAndTablePanel.setLayout(new BoxLayout(buttonAndTablePanel, BoxLayout.Y_AXIS));

        JPanel topButtonsPanel = new JPanel();
        JButton showDetailBtn = new JButton("Afficher les détails");
        ManageSectionWindow thisView = this;
        showDetailBtn.addActionListener(e -> {
            if (selectedItemId == -1) {
                JOptionPane.showMessageDialog(null,
                        notSelectedErrorMessage, alertTitle,
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            frame.setVisible(false);
            editDescriptionWindow = new EditDescriptionWindow(thisView);
            editDescriptionWindow.getFrame().setVisible(true);

        });
        JPanel secondPanel = new JPanel();

        JButton createItemBtn = new JButton("Créer un article");
        createItemBtn.addActionListener(e -> {
            frame.setVisible(false);
            createAnItemWindow = new CreateAnItemWindow(thisView);
            createAnItemWindow.getFrame().setVisible(true);
        });

        JButton deleteItemBtn = new JButton("Supprimer cet article");

        deleteItemBtn.addActionListener(e -> {
            if (selectedItemId == -1) {
                JOptionPane.showMessageDialog(null,
                        notSelectedErrorMessage, alertTitle,
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            int n = JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer cet article?",
                    "Supprimer un article", JOptionPane.YES_NO_OPTION);
            if (n == JOptionPane.YES_OPTION) {
                ItemController.removeItem(selectedItemId);
                refreshItemTable();
            }


        });
        secondPanel.add(createItemBtn);
        secondPanel.add(deleteItemBtn);

        JButton retournerButton = new JButton("Retourner");
        retournerButton.addActionListener(e -> {
            frame.setVisible(false);
            if (managerHomePage != null) {
                managerHomePage.getFrame().setVisible(true);
                return;
            }
            sellerHomePage.getFrame().setVisible(true);


        });
        JPanel midButtonsPanel = new JPanel();

        JButton incrementBtn = new JButton("Incrémenter cet article");

        JButton decreaseBtn = new JButton("Décrémenter cet article");
        JButton refreshBtn = new JButton("Rafraîchir");
        refreshBtn.addActionListener(e -> {
            refreshItemTable();
            selectedItemId = -1;
        });

        JPanel underButtonsPanel = new JPanel();
        JLabel itemNbLabel = new JLabel("Entrer le nombre d'articles à ajouter ou à supprimer");
        JTextField nbInput = new JTextField();
        nbInput.setColumns(15);
        incrementBtn.addActionListener(e -> {

            String inputNbString = nbInput.getText();
            RESULT result = ItemController.incrementItemStock(selectedItemId, inputNbString);
            switch (result) {
                case NOT_SELECTED_ERROR:
                    JOptionPane.showMessageDialog(frame,
                            notSelectedErrorMessage, alertTitle,
                            JOptionPane.ERROR_MESSAGE);
                    break;
                case SUCCESS:
                    JOptionPane.showMessageDialog(frame, "Incrémenter stock avec succès", successTitle,
                            JOptionPane.INFORMATION_MESSAGE);
                    break;
                case NUMBER_FORMAT_EXCEPTION:
                    JOptionPane.showMessageDialog(frame, "Le nombre entrée n'est pas un nombre valide", alertTitle,
                            JOptionPane.ERROR_MESSAGE);
                    break;

                default:
                    break;
            }

        });
        decreaseBtn.addActionListener(e -> {
            String inputNbString = nbInput.getText();
            RESULT result = ItemController.decrementItemStock(selectedItemId, inputNbString);

            switch (result) {
                case NOT_SELECTED_ERROR:
                    JOptionPane.showMessageDialog(frame,
                            notSelectedErrorMessage, alertTitle,
                            JOptionPane.ERROR_MESSAGE);
                    break;
                case SUCCESS:
                    JOptionPane.showMessageDialog(frame, "Decrémenter stock avec succès", successTitle,
                            JOptionPane.INFORMATION_MESSAGE);
                    break;
                case NUMBER_FORMAT_EXCEPTION:
                    JOptionPane.showMessageDialog(frame, "Le nombre entrée n'est pas un nombre valide", alertTitle,
                            JOptionPane.ERROR_MESSAGE);
                    break;

                default:
                    break;
            }

        });
        topButtonsPanel.add(retournerButton);
        topButtonsPanel.add(showDetailBtn);

        midButtonsPanel.add(incrementBtn);
        midButtonsPanel.add(decreaseBtn);
        midButtonsPanel.add(refreshBtn);
        underButtonsPanel.add(itemNbLabel);
        underButtonsPanel.add(nbInput);

        buttonAndTablePanel.add(topButtonsPanel);
        buttonAndTablePanel.add(secondPanel);

        buttonAndTablePanel.add(underButtonsPanel);
        buttonAndTablePanel.add(midButtonsPanel);

        setUpItemsTable();

        JScrollPane scrollPane = new JScrollPane(itemsTable);
        buttonAndTablePanel.add(scrollPane);
        mainPanel.add(buttonAndTablePanel);
        frame.getContentPane().add(mainPanel);

    }

    private void setUpItemsTable() {
        itemsTable = new JTable();
        refreshItemTable();
        itemsTable.getTableHeader().setReorderingAllowed(false);
        itemsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        itemsTable.getSelectionModel().addListSelectionListener(e -> {
            int row = itemsTable.getSelectedRow();
            if (row != -1 && !e.getValueIsAdjusting()) {

                Object idObj = table_model.getValueAt(row, 0);
                selectedItemId = Integer.parseInt(idObj.toString());

            }
        });
    }

    /**
     * Update the contents of the item table.
     */
    private void refreshItemTable() {

        table_model = getTheSectionTableModel();

        itemsTable.setModel(table_model);

        table_model.addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.UPDATE) {
                int firstRow = e.getFirstRow();
                int lastRow = e.getLastRow();
                for (int row = firstRow; row <= lastRow; row++) {
                    Object idObj = itemsTable.getModel().getValueAt(row, 0);
                    int id = Integer.parseInt(idObj.toString());
                    Object nomObj = itemsTable.getModel().getValueAt(row, 1);
                    String nom = nomObj.toString();
                    Object typeObj = itemsTable.getModel().getValueAt(row, 2);
                    String type = typeObj.toString();
                    Object stockObj = itemsTable.getModel().getValueAt(row, 3);
                    RESULT res = ItemController.updateItem(id, nom, type, stockObj.toString());
                    switch (res) {
                        case SUCCESS:
                            JOptionPane.showMessageDialog(frame, "Rafraichir article avec succès", successTitle,
                                    JOptionPane.INFORMATION_MESSAGE);
                            break;
                        case NUMBER_FORMAT_EXCEPTION:
                            JOptionPane.showMessageDialog(frame, "Stock n'est pas un nombre valide", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        default:
                            break;
                    }

                }

            }
        });


    }

    /**
     * Update the DefaultTableModel of the item table.
     */
    private DefaultTableModel getTheSectionTableModel() {

        List<Item> articles = ItemDao.getItemList(sectionId);
        if (articles == null) {
            return new ItemTableModel(null,
                    new String[]{"Id_Article", "Nom du article", "Type d'articles", "Stock"});
        }
        Object[][] ob = new Object[articles.size()][4];
        Iterator<Item> it = articles.iterator();
        int n = 0;
        while (it.hasNext()) {
            Item A = it.next();
            ob[n][0] = String.valueOf(A.getId());
            ob[n][1] = A.getitemName();
            ob[n][2] = A.getType();
            ob[n][3] = A.getStock();
            n++;

        }
        return new ItemTableModel(ob, new String[]{"Id_Article", "Nom du article", "Type d'articles", "Stock"});
    }

    /**
     * Gets selectedItemId.
     *
     * @return A item id that selected by user from table.
     */
    public int getSelectedItemId() {
        return selectedItemId;
    }

    /**
     * Gets sectionId.
     *
     * @return A section id that selected by user from table.
     */
    public int getSectionId() {
        return sectionId;
    }

    /**
     * <p>
     * ItemTableModel is the class representing the customized model of itemsTable.
     * </p>
     */
    @SuppressWarnings("serial")
    class ItemTableModel extends DefaultTableModel {

        private ItemTableModel(Object[][] cellData, String[] columnNames) {
            super(cellData, columnNames);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return column != 0;
        }

    }

}
