package view;

import controller.UserController;
import enums.RESULT;

import javax.swing.*;

/**
 * <p>
 * CreateAUserWindow is the class representing the window that opens when a
 * manager create a user.
 * </p>
 * 
 * <p>
 * A CreateAUserWindow is characterized by the following information :
 * <ul>
 * <li>A ManagerMainWindow that the CreateAUserWindow will return back
 * to.</li>
 * <li>A JFrame contains all other required components.</li>
 * <li>A field that allows to enter the user's first name.</li>
 * <li>A field that allows to enter the user's last name.</li>
 * <li>A field that allows to enter the user's password.</li>
 * <li>A field that allows to enter the user's section name.</li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class CreateAUserWindow {
	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;
	
	/**
	 * A field that allows to enter user's last name.
	 */
	private JTextField lastNameTextField;
	
	/**
	 * A field that allows to enter the user's first name.
	 */
	private JTextField firstNameTextField;
	
	/**
	 * A field that allows to enter the user's password.
	 */
	private JTextField passwordTextField;
	
	/**
	 * A field that allows to enter the user's section name.
	 */
	private JTextField sectionTextField;
	
	/**
	 * A ManagerMainWindow that the CreateAUserWindow will return back to.
	 */
	private ManagerMainWindow managerMainWindow;

	/**
	 * A parameterized constructor.
	 * 
	 * @param view An object of the class ManagerMainWindow
	 */
	public CreateAUserWindow(ManagerMainWindow view) {
		managerMainWindow = view;
		initialize();
	}
	
	/**
	 * Gets JFrame.
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lastNameLabel = new JLabel("Nom");
		lastNameLabel.setBounds(59, 42, 61, 16);
		frame.getContentPane().add(lastNameLabel);

		JLabel firstNameLabel = new JLabel("Prénom");
		firstNameLabel.setBounds(59, 91, 61, 16);
		frame.getContentPane().add(firstNameLabel);

		lastNameTextField = new JTextField();
		lastNameTextField.setBounds(172, 37, 130, 26);
		frame.getContentPane().add(lastNameTextField);
		lastNameTextField.setColumns(10);

		firstNameTextField = new JTextField();
		firstNameTextField.setBounds(172, 86, 130, 26);
		frame.getContentPane().add(firstNameTextField);
		firstNameTextField.setColumns(10);

		JLabel passwordLabel = new JLabel("Mot de passe");
		passwordLabel.setBounds(59, 146, 96, 16);
		frame.getContentPane().add(passwordLabel);

		passwordTextField = new JTextField();
		passwordTextField.setBounds(172, 141, 130, 26);
		frame.getContentPane().add(passwordTextField);
		passwordTextField.setColumns(10);

		JLabel sectionNameLabel = new JLabel("Nom du rayon");
		sectionNameLabel.setBounds(59, 199, 96, 16);
		frame.getContentPane().add(sectionNameLabel);

		sectionTextField = new JTextField();
		sectionTextField.setBounds(172, 194, 130, 26);
		frame.getContentPane().add(sectionTextField);
		sectionTextField.setColumns(10);

		JButton createBtn = new JButton("Créer");
		createBtn.addActionListener(e -> {

			String lastName = lastNameTextField.getText();
			String firtName = firstNameTextField.getText();
			String password = passwordTextField.getText();
			String sectionName = sectionTextField.getText();
			RESULT res = UserController.createUser(lastName, firtName, sectionName, password);
			switch (res) {
			case SUCCESS:
				JOptionPane.showMessageDialog(frame, "Ajouté avec succès", "succès",
						JOptionPane.INFORMATION_MESSAGE);
				break;
			case EMPTY_ERROR:
				JOptionPane.showMessageDialog(frame,
						"le nom,prenom et mot de passe du utilisateur ne peut pas être vide", "alert",
						JOptionPane.ERROR_MESSAGE);
				break;
			case DUPLICATE_ERROR:
				JOptionPane.showMessageDialog(frame, "Le nom d'utilisateur existe déjà", "alert",
						JOptionPane.ERROR_MESSAGE);
				break;

			default:
				break;
			}

		});
		createBtn.setBounds(59, 232, 117, 29);
		frame.getContentPane().add(createBtn);

		JButton cancelBtn = new JButton("Annuler");
		cancelBtn.addActionListener(e -> {
			frame.setVisible(false);
			managerMainWindow.getFrame().setVisible(true);
		});
		cancelBtn.setBounds(261, 232, 117, 29);
		frame.getContentPane().add(cancelBtn);
	}
}
