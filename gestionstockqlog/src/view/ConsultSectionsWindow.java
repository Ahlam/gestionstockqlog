package view;

import controller.SectionController;
import model.Item;
import model.Section;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ItemEvent;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * ConsultSectionsWindow is the class representing the window that opens when a
 * seller consult sections.
 * </p>
 * 
 * <p>
 * A ConsultSectionsWindow is characterized by the following information :
 * <ul>
 * <li>A SellerMainWindow that contains the ConsultSectionsWindow's view.</li>
 * <li>A JFrame contains all other required components.</li>
 * <li>A string that allows to track section selected by seller from drop-down
 * list.</li>
 * <li>A drop-down list of sections.</li>
 * <li>A table of items.</li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class ConsultSectionsWindow {

	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;

	/**
	 * A table of items.
	 */
	private JTable itemTable;

	/**
	 * Allows to back to the SellerMainWindow.
	 */
	private SellerMainWindow mainWindow;

	/**
	 * A drop-down list of section DefaultComboBoxModel.
	 */
	private DefaultComboBoxModel<String> sectionComboModel;

	/**
	 * A string that allows to track section selected by seller from drop-down list.
	 */
	private String selectedSectionName;

	/**
	 * A parameterized constructor.
	 * 
	 * @param mainWindow An object of the class SellerMainWindow
	 */
	public ConsultSectionsWindow(SellerMainWindow mainWindow) {
		this.mainWindow = mainWindow;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		JPanel headPanel = new JPanel();
		JLabel sectionNameLabel = new JLabel("Nom du rayon");
		JButton backButton = new JButton("Retourner");

		backButton.addActionListener(e -> {

			frame.setVisible(false);
			mainWindow.getFrame().setVisible(true);
		});

		headPanel.add(backButton);
		headPanel.add(sectionNameLabel);

		JPanel itemTablePanel = new JPanel();

		itemTable = new JTable(getItemsTableModel());
		JScrollPane scrollPane = new JScrollPane(itemTable);
		itemTablePanel.add(scrollPane);
		frame.getContentPane().add(headPanel);

		JComboBox<String> comboBox = new JComboBox<String>();
		sectionComboModel = getComboBoxModel();
		comboBox.setModel(sectionComboModel);

		comboBox.addItemListener(e -> {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				selectedSectionName = e.getItem().toString();
				refreshTable();
			}
		});
		headPanel.add(comboBox);
		frame.getContentPane().add(itemTablePanel);

	}

	/**
	 * Updates the itemsTable.
	 */
	private void refreshTable() {
		itemTable.setModel(getItemsTableModel());
	}

	/**
	 * Initialize the ItemsTableModel .
	 * 
	 * @return the DefaultTableModel of items.
	 */
	private DefaultTableModel getItemsTableModel() {
		Section section = SectionController.getSection(selectedSectionName);

		if (section == null) {
			return new UnchangeableItemsTableModel(null,
					new String[] { "Id_Article", "Nom du article", "Type d'articles", "Stock" });
		}
		Object[][] ob = new Object[section.getArticles().size()][4];
		Iterator<Item> it = section.getArticles().iterator();
		int n = 0;
		while (it.hasNext()) {
			Item A = it.next();
			ob[n][0] = String.valueOf(A.getId());
			ob[n][1] = A.getitemName();
			ob[n][2] = A.getType();
			ob[n][3] = A.getStock();
			n++;

		}
		return new UnchangeableItemsTableModel(ob,
				new String[] { "Id_Article", "Nom du article", "Type d'articles", "Stock" });
	}

	/**
	 * <p>
	 * UnchangeableItemsTableModel is the class representing the read-only model of
	 * itemsTable.
	 * </p>
	 *
	 */
	@SuppressWarnings("serial")
	class UnchangeableItemsTableModel extends DefaultTableModel {

		private UnchangeableItemsTableModel(Object[][] cellData, String[] columnNames) {
			super(cellData, columnNames);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	}

	/**
	 * Initialize the DefaultComboBoxModel .
	 * 
	 * @return the DefaultComboBoxModel of section name.
	 */
	private DefaultComboBoxModel<String> getComboBoxModel() {

		List<Section> sections = SectionController.getAllSections();

		String[] resStrings = new String[sections.size()];
		Iterator<Section> it = sections.iterator();
		int n = 0;
		while (it.hasNext()) {
			Section section = it.next();
			resStrings[n] = section.getSectionName();
			n++;

		}
		return new DefaultComboBoxModel<String>(resStrings);
	}

	/**
	 * Gets JFrame.
	 * 
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}
}
