package view;

import dao.UserDao;
import enums.FUNCTION;
import model.User;

import javax.swing.*;

/**
 * <p>
 * LoginWindow is the class representing the window that opens when a user login
 * in .
 * </p>
 * 
 * <p>
 * A LoginWindow is characterized by the following information :
 * <ul>
 * <li>A ManageSectionWindow that the EditDescriptionWindow will return back
 * to.</li>
 * <li>A JFrame contains all other required components.</li>
 * <li>A field that allows to enter the user's id.</li>
 * <li>A field that allows to enter the user's password.</li>
 * <li>A SellerMainWindow that the seller will get after successful login</li>
 * <li>A ManagerMainWindow that the manager will get after successful
 * login</li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class LoginWindow {
	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;

	/**
	 * A field that allows to enter the user's id.
	 */
	private JTextField userIdTextField;

	/**
	 * A field that allows to enter the user's id.
	 */
	private JPasswordField passwordField;

	/**
	 * A SellerMainWindow that the seller will get after successful login.
	 */
	private SellerMainWindow sellerHomePage;

	/**
	 * A ManagerMainWindow that the manager will get after successful login.
	 */
	private ManagerMainWindow managerHomePage;



	/**
	 * A default constructor.
	 *
	 */
	public LoginWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		userIdTextField = new JTextField();
		userIdTextField.setBounds(177, 75, 130, 26);
		frame.getContentPane().add(userIdTextField);
		userIdTextField.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(177, 133, 130, 26);
		frame.getContentPane().add(passwordField);
		JLabel warningLabel = new JLabel("");
		warningLabel.setBounds(79, 40, 309, 16);
		frame.getContentPane().add(warningLabel);
		JButton btnLogIn = new JButton("Se connecter");
		btnLogIn.addActionListener(e -> {
			String idStr = userIdTextField.getText();

			String passwordStr = new String(passwordField.getPassword());
			if (idStr.isEmpty() || passwordStr.isEmpty()) {
				warningLabel.setText("Identifiant ou mot de passe est obsente");
				return;
			}

			try {
				int id = Integer.parseInt(idStr);
				if (UserDao.isValidUser(id, passwordStr)) {
					User u = UserDao.getUser(id);
					frame.setVisible(false);
					if (u.getFunction() == FUNCTION.MANAGER) {

						managerHomePage = new ManagerMainWindow();
						managerHomePage.getFrame().setVisible(true);
					} else {
						sellerHomePage = new SellerMainWindow();
						sellerHomePage.getFrame().setVisible(true);
						sellerHomePage.setSellerId(id);
					}
					return;
				}
				warningLabel.setText("Identifiant ou mot de passe n'est pas correcte");


			} catch (NumberFormatException e2) {
				warningLabel.setText("Identifiant  n'est pas un nombre");
			}



		});
		btnLogIn.setBounds(190, 198, 117, 29);
		frame.getContentPane().add(btnLogIn);

		JLabel passwordLabel = new JLabel("Mot de passe");
		passwordLabel.setBounds(79, 138, 86, 16);
		frame.getContentPane().add(passwordLabel);

		JLabel userIdLabel = new JLabel("Identifiant");
		userIdLabel.setBounds(79, 80, 86, 16);
		frame.getContentPane().add(userIdLabel);

	}
	
	/**
	 * Gets JFrame.
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}
}
