package view;

import controller.SectionController;
import controller.UserController;
import dao.SectionDao;
import dao.UserDao;
import enums.FUNCTION;
import enums.RESULT;
import model.Section;
import model.User;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * ManagerMainWindow is the class representing the window that opens when a
 * manager successful login.
 * </p>
 *
 * <p>
 * A ManagerMainWindow is characterized by the following information :
 * <ul>
 * <li>A JFrame contains all other required components.</li> *
 * <li>A table of sections.</li>
 * <li>A DefaultTableModel of sectionsTable.</li> *
 * <li>A table of users.</li>
 * <li>A DefaultTableModel of usersTable.</li>
 * <li>A integer that allows to track user selected by user from table.</li>
 * <li>A integer that allows to track section selected by user from table.</li>
 * <li>The window for creating a user .</li>
 * <li>The window for creating a section .</li>
 * <li>The window for managing a section .</li>
 * </ul>
 * </p>
 *
 * @author Yang YANG
 */
public class ManagerMainWindow {
    /**
     * A JFrame contains all other required components in this window.
     */
    private JFrame frame;

    /**
     * A table of sections.
     */
    private JTable sectionsTable;

    /**
     * A table of users.
     */
    private JTable usersTable;

    /**
     * A DefaultTableModel of sectionsTable.
     */
    private DefaultTableModel sectionsTable_model;

    /**
     * A DefaultTableModel of usersTable.
     */
    private DefaultTableModel usersTable_model;

    /**
     * A integer that allows to track user selected by user from table.
     */
    private int selectedUserId = -1;

    /**
     * A integer that allows to track section selected by user from table.
     */
    private int selectedSectionId = -1;

    /**
     * The window for creating a user.
     */
    private CreateAUserWindow createAUserWindow;

    /**
     * The window for creating a section.
     */
    private CreateASectionWindow createASectionWindow;

    /**
     * The window for managing a section.
     */
    private ManageSectionWindow manageSectionWindow;
    /**
     * A message when found no selected a item or article.
     */
    private String notSelectedErrorMessage = "Faut d'abord sélectionner une ligne de données dans le tableau";
    /**
     * A title for alert window.
     */
    private String alertTitle = "Alert";

    /**
     * A default constructor.
     */
    public ManagerMainWindow() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        ManagerMainWindow thisView = this;
        frame.setBounds(100, 100, 570, 383);
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JTabbedPane tabbedPane = new JTabbedPane(javax.swing.SwingConstants.TOP);
        frame.getContentPane().add(tabbedPane);
        JPanel sectionJpanel = new JPanel();
        sectionJpanel.setLayout(new BoxLayout(sectionJpanel, BoxLayout.Y_AXIS));

        JPanel sectionBtnsPanel = new JPanel();

        JButton createSectionBtn = new JButton("Créer un rayon");
        createSectionBtn.addActionListener(e -> {
            frame.setVisible(false);
            createASectionWindow = new CreateASectionWindow(thisView);
            createASectionWindow.getFrame().setVisible(true);
        });

        JButton deleteSectionBtn = new JButton("Supprimer ce rayon");
        deleteSectionBtn.addActionListener(e -> {
            if (selectedSectionId == -1) {
                JOptionPane.showMessageDialog(null,
                        notSelectedErrorMessage, alertTitle,
                        JOptionPane.ERROR_MESSAGE);
            } else {
                int n = JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer ce rayon?",
                        "Supprimer un rayon", JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    SectionDao.removeSection(selectedSectionId);
                    refreshSectionsTable();
                    selectedSectionId = -1;
                }
            }

        });

        JButton refreshSectionsTableBtn = new JButton("Rafraîchir");

        sectionBtnsPanel.add(createSectionBtn);
        sectionBtnsPanel.add(deleteSectionBtn);
        sectionBtnsPanel.add(refreshSectionsTableBtn);
        refreshSectionsTableBtn.addActionListener(e -> {
            refreshSectionsTable();
            selectedSectionId = -1;
        });

        sectionJpanel.add(sectionBtnsPanel);

        setUpSectionsTable(thisView);

        JScrollPane sectionScrollPane = new JScrollPane(sectionsTable);
        sectionJpanel.add(sectionScrollPane);
        tabbedPane.addTab("Gestion des rayons", null, sectionJpanel, null);

        JPanel jpanelUser = new JPanel();
        jpanelUser.setLayout(new BoxLayout(jpanelUser, BoxLayout.Y_AXIS));

        setUpUserTable();

        JScrollPane userScrollPane = new JScrollPane(usersTable);
        JPanel buttonsPanelUser = new JPanel();
        JButton CreateUser = new JButton("Créer un utilisateur");

        CreateUser.addActionListener(e -> {
            frame.setVisible(false);
            createAUserWindow = new CreateAUserWindow(thisView);
            createAUserWindow.getFrame().setVisible(true);
        });
        JButton DeleteUser = new JButton("Supprimer cet utilisateur");
        DeleteUser.addActionListener(e -> {
            if (selectedUserId == -1) {
                JOptionPane.showMessageDialog(null,
                        notSelectedErrorMessage, alertTitle,
                        JOptionPane.ERROR_MESSAGE);
            } else {
                int n = JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir supprimer cet utilisateur?",
                        "Supprimer un utilisateur", JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    UserDao.removeUser(selectedUserId);
                    refreshUsersTable();
                    selectedUserId = -1;
                }
            }

        });

        JButton refreshUsersTableBtn = new JButton("Rafraîchir");
        refreshUsersTableBtn.addActionListener(e -> refreshUsersTable());
        buttonsPanelUser.add(CreateUser);
        buttonsPanelUser.add(DeleteUser);
        buttonsPanelUser.add(refreshUsersTableBtn);

        jpanelUser.add(buttonsPanelUser);
        jpanelUser.add(userScrollPane);
        tabbedPane.addTab("Gestion des utilisateurs", null, jpanelUser, null);

    }

    private void setUpSectionsTable(ManagerMainWindow thisView) {
        sectionsTable = new JTable();
        refreshSectionsTable();
        sectionsTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(final MouseEvent arg0) {
                if (arg0.getButton() == 3) {
                    if (selectedSectionId == -1) {
                        JOptionPane.showMessageDialog(null,
                                notSelectedErrorMessage, alertTitle,
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        manageSectionWindow = new ManageSectionWindow(thisView, selectedSectionId);
                        manageSectionWindow.getFrame().setVisible(true);
                        thisView.getFrame().setVisible(false);
                    }

                }

            }
        });
        sectionsTable.getTableHeader().setReorderingAllowed(false);
        sectionsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        sectionsTable.getSelectionModel().addListSelectionListener(e -> {
            int row = sectionsTable.getSelectedRow();
            if (row != -1 && !e.getValueIsAdjusting()) {

                Object idObj = sectionsTable_model.getValueAt(row, 0);
                selectedSectionId = Integer.parseInt(idObj.toString());


            }
        });
    }

    private void setUpUserTable() {
        usersTable = new JTable();
        refreshUsersTable();
        usersTable.getTableHeader().setReorderingAllowed(false);
        usersTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        usersTable.getSelectionModel().addListSelectionListener(e -> {
            int row = usersTable.getSelectedRow();
            if (row != -1 && !e.getValueIsAdjusting()) {

                Object idObj = usersTable_model.getValueAt(row, 0);
                selectedUserId = Integer.parseInt(idObj.toString());


            }
        });
    }

    /**
     * Gets JFrame.
     *
     * @return A JFrame contains all other required components.
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Gets DefaultTableModel.
     *
     * @return A DefaultTableModel of users table.
     */
    private DefaultTableModel getUserTableModel() {
        List<User> users = UserDao.getUserList();
        Object[][] ob = new Object[users.size()][6];
        Iterator<User> it = users.iterator();
        int n = 0;
        while (it.hasNext()) {
            User user = it.next();
            ob[n][0] = String.valueOf(user.getId());
            ob[n][1] = user.getLastName();
            ob[n][2] = user.getFirtsName();
            if (user.getSection() == null) {
                if (user.getFunction() == FUNCTION.MANAGER) {
                    ob[n][3] = "Tout";
                } else {
                    ob[n][3] = "Non attribué";
                }

            } else {
                ob[n][3] = user.getSection().getSectionName();
            }

            ob[n][4] = user.getPassword();
            ob[n][5] = user.getFunction();
            n++;

        }
        return new UserTableModel(ob,
                new String[]{"Id_utilisateur", "Nom", "PrélastName", "Section", "Mot de passe", "Fonction"});
    }

    /**
     * Update the contents of users table.
     */
    private void refreshUsersTable() {
        usersTable_model = getUserTableModel();
        usersTable.setModel(usersTable_model);

        usersTable_model.addTableModelListener(e -> {

            if (e.getType() == TableModelEvent.UPDATE) {
                int firstRow = e.getFirstRow();
                int lastRow = e.getLastRow();
                for (int row = firstRow; row <= lastRow; row++) {
                    Object idObj = usersTable_model.getValueAt(row, 0);
                    int id = Integer.parseInt(idObj.toString());
                    Object lastNameObj = usersTable_model.getValueAt(row, 1);
                    String lastName = lastNameObj.toString();

                    Object firstNameObj = usersTable_model.getValueAt(row, 2);
                    String firstName = firstNameObj.toString();

                    Object sectionObj = usersTable_model.getValueAt(row, 3);
                    String section = sectionObj.toString();

                    Object passwordObj = usersTable_model.getValueAt(row, 4);
                    String password = passwordObj.toString();

                    Object functionObject = usersTable_model.getValueAt(row, 5);
                    String function = functionObject.toString();

                    RESULT result = UserController.updateUser(id, lastName, firstName, section, password, function);
                    switch (result) {
                        case SUCCESS:
                            JOptionPane.showMessageDialog(frame, "Succès de mise à jour cet utilisateur", "succès",
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        case ALLOCATION_ERROR:
                            JOptionPane.showMessageDialog(frame,
                                    "Le rayon du resposable ne peut pas être modifié ", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        case NO_RESULT_FOUND_ERROR:
                            JOptionPane.showMessageDialog(null, "Impossible de trouver ce rayon <<" + section
                                    + ">> dans la base de données", alertTitle, JOptionPane.ERROR_MESSAGE);
                            break;
                        case EMPTY_ERROR:
                            JOptionPane.showMessageDialog(frame,
                                    "Le lastName ,le prélastName et le mot de passe du utilisateur ne peut pas être vide",
                                    alertTitle, JOptionPane.ERROR_MESSAGE);
                            break;

                        default:
                            break;
                    }

                }

            }
        });
    }

    /**
     * Gets DefaultTableModel.
     *
     * @return A DefaultTableModel of sections table.
     */
    private DefaultTableModel getSectionTableModel() {
        List<Section> sections = SectionDao.getSectionList();
        Object[][] ob = new Object[sections.size()][3];
        Iterator<Section> it = sections.iterator();
        int n = 0;
        while (it.hasNext()) {
            Section section = it.next();
            ob[n][0] = String.valueOf(section.getId());
            ob[n][1] = section.getSectionName();
            if (section.getSeller() == null) {
                ob[n][2] = " Non attribué";
            } else {
                ob[n][2] = section.getSeller().getFirtsName() + " " + section.getSeller().getLastName();
            }

            n++;

        }
        return new SectionTableModel(ob, new String[]{"Id_Rayon", "Nom du rayon", "Chef du rayon"});
    }

    /**
     * Update the contents of users table.
     */
    private void refreshSectionsTable() {
        sectionsTable_model = getSectionTableModel();
        sectionsTable.setModel(sectionsTable_model);

        sectionsTable_model.addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.UPDATE) {
                int firstRow = e.getFirstRow();
                int lastRow = e.getLastRow();
                for (int row = firstRow; row <= lastRow; row++) {
                    Object idObj = sectionsTable_model.getValueAt(row, 0);
                    int id = Integer.parseInt(idObj.toString());
                    Object lastNameObj = sectionsTable_model.getValueAt(row, 1);
                    String sectionName = lastNameObj.toString();

                    Object managerObj = sectionsTable_model.getValueAt(row, 2);
                    String managerName = managerObj.toString();

                    RESULT result = SectionController.updateSection(id, sectionName, managerName);
                    switch (result) {
                        case EMPTY_ERROR:
                            JOptionPane.showMessageDialog(frame, "Le nom du rayon ne peut pas être vide", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        case SUCCESS:
                            JOptionPane.showMessageDialog(frame, "Succès de mise à jour de ce rayon", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        case NO_RESULT_FOUND_ERROR:
                            JOptionPane.showMessageDialog(frame,
                                    "Cet responsable n'existe pas dans la base de données，Veuillez entrer dans l'ordre ：prénom+un espace+nom",
                                    alertTitle, JOptionPane.ERROR_MESSAGE);
                            break;
                        case ALLOCATION_ERROR:
                            JOptionPane.showMessageDialog(frame,
                                    "Les rayons ne peuvent pas être attribué à un resposable", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;
                        case MANAGER_NAME_LENGTH_ERROR:
                            JOptionPane.showMessageDialog(frame,
                                    "Le nom du chef ne doit contenir que le prénom et le nom", alertTitle,
                                    JOptionPane.ERROR_MESSAGE);
                            break;

                        default:
                            break;
                    }

                }

            }
        });
    }

    /**
     * <p>
     * SectionTableModel is the class representing the customized model of sections
     * table.
     * </p>
     */
    @SuppressWarnings("serial")
    class SectionTableModel extends DefaultTableModel {

        private SectionTableModel(Object[][] cellData, String[] columnNames) {
            super(cellData, columnNames);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return column != 0;
        }

    }

    /**
     * <p>
     * UserTableModel is the class representing the customized model of users table.
     * </p>
     */
    @SuppressWarnings("serial")
    class UserTableModel extends DefaultTableModel {

        private UserTableModel(Object[][] cellData, String[] columnNames) {
            super(cellData, columnNames);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return column != 0 && column != 5;
        }

    }

}
