package view;

import controller.ItemController;

import javax.swing.*;
/**
 * <p>
 * EditDescriptionWindow is the class representing the window that opens when a
 * seller edit the description of a item.
 * </p>
 * 
 * <p>
 * A EditDescriptionWindow is characterized by the following information :
 * <ul>
 * <li>A ManageSectionWindow that the EditDescriptionWindow will return back
 * to.</li>
 * <li>A JFrame contains all other required components.</li>
 * 
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class EditDescriptionWindow {
	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;
	
	/**
	 * A ManageSectionWindow that the EditDescriptionWindow will return back to.
	 */
	private ManageSectionWindow managerHomePage;
	
	/**
	 * Gets JFrame.
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}
	/**
	 * A parameterized constructor.
	 * 
	 * @param managerHomePage An object of the class ManageSectionWindow
	 */
	public EditDescriptionWindow(ManageSectionWindow managerHomePage) {
		this.managerHomePage=managerHomePage;
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLaDescription = new JLabel("La description");
		lblLaDescription.setBounds(63, 91, 101, 16);
		frame.getContentPane().add(lblLaDescription);
		
		JTextArea textArea = new JTextArea();
		
		int articleId = managerHomePage.getSelectedItemId();
		String descriptionString = ItemController.getItemDescription(articleId);
		textArea.setText(descriptionString);
		
		textArea.setBounds(187, 91, 146, 86);
		frame.getContentPane().add(textArea);
		
		JButton btnValiderLaModification = new JButton("Valider la modification");
		btnValiderLaModification.setBounds(42, 213, 191, 29);
		btnValiderLaModification.addActionListener(e -> {
			frame.setVisible(false);
			managerHomePage.getFrame().setVisible(true);
			String descriptionString1 =textArea.getText();
			ItemController.updateItemDescription(articleId, descriptionString1);

		});
		frame.getContentPane().add(btnValiderLaModification);
		
		JButton btnAnunler = new JButton("Annuler");
		btnAnunler.addActionListener(e -> {
			frame.setVisible(false);
			managerHomePage.getFrame().setVisible(true);
		});
		btnAnunler.setBounds(245, 213, 117, 29);
		frame.getContentPane().add(btnAnunler);
	}
}
