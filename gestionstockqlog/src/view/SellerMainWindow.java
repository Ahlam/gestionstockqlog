package view;

import javax.swing.*;

/**
 * <p>
 * SellerMainWindow is the class representing the window that opens when a
 * seller successful login.
 * </p>
 * 
 * <p>
 * A SellerMainWindow is characterized by the following information :
 * <ul>
 * <li>A JFrame contains all other required components.</li>
 * <li>The window for manager its section.</li>
 * <li>The window for consulting other sections.</li>
 * <li>The window for consulting other sections.</li>
 * <li>The seller id.</li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class SellerMainWindow {
	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;

	/**
	 * The window for manager its section.
	 */
	private ManageSectionWindow GMRwindow;

	/**
	 * The window for consulting other sections.
	 */
	private ConsultSectionsWindow CDRwindow;

	/**
	 * The seller id.
	 */
	private int sellerId;


	/**
	 * Gets JFrame.
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}

	/**
	 * The default constructor.
	 */
	public SellerMainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();

		GMRwindow = new ManageSectionWindow(this, sellerId);
		CDRwindow = new ConsultSectionsWindow(this);
		frame.setBounds(100, 100, 646, 417);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 646, 389);

		JButton btnGestionMonRayon = new JButton("Gérer mon rayon");

		btnGestionMonRayon.addActionListener(e -> {

			frame.setVisible(false);
			GMRwindow.getFrame().setVisible(true);
		});
		btnGestionMonRayon.setBounds(83, 89, 187, 29);
		panel.add(btnGestionMonRayon);

		JButton btnConsulterDautreRayon = new JButton("Consulter d'autres rayons");
		btnConsulterDautreRayon.addActionListener(e -> {

			frame.setVisible(false);
			CDRwindow.getFrame().setVisible(true);
		});

		btnConsulterDautreRayon.setBounds(331, 89, 213, 29);
		panel.add(btnConsulterDautreRayon);
		frame.getContentPane().add(panel);
	}

	/**
	 * Modify the seller id.
	 */
	public void setSellerId(int sellerId) {
		this.sellerId = sellerId;
	}

}
