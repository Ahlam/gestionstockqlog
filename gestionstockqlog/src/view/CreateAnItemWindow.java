package view;

import controller.ItemController;
import enums.RESULT;

import javax.swing.*;


/**
 * <p>
 * CreateAnItemWindow is the class representing the window that opens when a seller create an item in his section.
 * </p>
 * 
 * <p>
 * A CreateAnItemWindow is characterized by the following information :
 * <ul>
 * <li> A ManageSectionWindow that the CreateAnItemWindow will return back to. </li>
 * <li> A JFrame contains all other required components. </li>
 * <li> A integer that allows to track section selected by seller from table. </li>
 * <li> A field that allows to enter the item's name. </li>
 * <li> A field that allows to enter the item's stock. </li>
 * <li> A field that allows to enter the item's type. </li>
 * </ul> 
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class CreateAnItemWindow {
	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;
	
	/**
	 * A field that allows to enter the item's name. 
	 */
	private JTextField itemNameTextField;
	
	/**
	 * A field that allows to enter the item's stock. 
	 */
	private JTextField stockTextField;
	
	/**
	 * A field that allows to enter the item's type. 
	 */
	private JTextField typeTextField;
	
	/**
	 * The window that the CreateAnItemWindow will return back to. 
	 */
	private ManageSectionWindow manageSectionWindow;
	
	/**
	 * A integer that allows to track section selected by seller from . . 
	 */
	private int selectedSectionId;

	/**
	 * A parameterized constructor.
	 * 
	 * @param thisView An object of the class ManageSectionWindow
	 */
	public CreateAnItemWindow(ManageSectionWindow thisView) {
		manageSectionWindow = thisView;
		this.selectedSectionId = thisView.getSectionId();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 503, 394);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel nameLabel = new JLabel("Nom de l'article");
		nameLabel.setBounds(88, 49, 114, 16);
		frame.getContentPane().add(nameLabel);

		JLabel typeLabel = new JLabel("Type de l'article");
		typeLabel.setBounds(88, 101, 114, 16);
		frame.getContentPane().add(typeLabel);

		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(88, 158, 61, 16);
		frame.getContentPane().add(lblStock);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(88, 207, 91, 16);
		frame.getContentPane().add(lblDescription);

		itemNameTextField = new JTextField();
		itemNameTextField.setBounds(220, 44, 130, 26);
		frame.getContentPane().add(itemNameTextField);
		itemNameTextField.setColumns(10);

		stockTextField = new JTextField();
		stockTextField.setBounds(220, 153, 130, 26);
		frame.getContentPane().add(stockTextField);
		stockTextField.setColumns(10);

		typeTextField = new JTextField();
		typeTextField.setBounds(220, 96, 130, 26);
		frame.getContentPane().add(typeTextField);
		typeTextField.setColumns(10);

		JTextArea textArea = new JTextArea();
		textArea.setBounds(220, 207, 136, 65);
		frame.getContentPane().add(textArea);

		JButton createBtn = new JButton("Créer");
		createBtn.addActionListener(e -> {
			String itemName = itemNameTextField.getText();
			String type = typeTextField.getText();
			String stockString = stockTextField.getText();
			String description = textArea.getText();
			RESULT res = ItemController.createItem(itemName, type, description, stockString, selectedSectionId);

			switch (res) {
				case SUCCESS:
					JOptionPane.showMessageDialog(frame, "Ajouté avec succès", "succès",
							JOptionPane.INFORMATION_MESSAGE);
					break;
				case EMPTY_ERROR:
					JOptionPane.showMessageDialog(frame, "le nom de l'article est vide", "alert",
							JOptionPane.ERROR_MESSAGE);
					break;
				case NUMBER_FORMAT_EXCEPTION:
					JOptionPane.showMessageDialog(frame, "Stock n'est pas un nombre valide", "alert",
							JOptionPane.ERROR_MESSAGE);
					break;
				default:
					break;
			}

		});

		createBtn.setBounds(85, 310, 117, 29);
		frame.getContentPane().add(createBtn);

		JButton cancelBtn = new JButton("Annuler");
		cancelBtn.addActionListener(e -> {
			frame.setVisible(false);
			manageSectionWindow.getFrame().setVisible(true);
		});
		cancelBtn.setBounds(303, 310, 117, 29);
		frame.getContentPane().add(cancelBtn);
	}
	
	/**
	 * Gets JFrame.
	 * @return A JFrame contains all other required components.
	 */
	public JFrame getFrame() {
		return frame;
	}

}
