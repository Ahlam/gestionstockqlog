package view;

import controller.SectionController;
import enums.RESULT;

import javax.swing.*;

/**
 * <p>
 * CreateASectionWindow is the class representing the window that opens when a
 * manager create a section.
 * </p>
 * 
 * <p>
 * A CreateASectionWindow is characterized by the following information :
 * <ul>
 * <li>A ManagerMainWindow that the CreateASectionWindow will return back
 * to.</li>
 * <li>A JFrame contains all other required components.</li>
 * <li>A field that allows to enter the section's name.</li>
 * </ul>
 * </p>
 * 
 * @author Yang YANG
 *
 */
public class CreateASectionWindow {

	/**
	 * A JFrame contains all other required components in this window.
	 */
	private JFrame frame;

	/**
	 * A field that allows to enter the section's name.
	 */
	private JTextField sectionNameTextField;

	/**
	 * The window that the CreateASectionWindow will return back to.
	 */
	private ManagerMainWindow managerMainWindow;

	public JFrame getFrame() {
		return frame;
	}

	/**
	 * A parameterized constructor.
	 * 
	 * @param managerMainWindow An object of the class ManagerMainWindow
	 */
	public CreateASectionWindow(ManagerMainWindow managerMainWindow) {
		this.managerMainWindow = managerMainWindow;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel sectionNameLabel = new JLabel("Nom du rayon:");
		sectionNameLabel.setBounds(65, 68, 136, 16);
		frame.getContentPane().add(sectionNameLabel);

		sectionNameTextField = new JTextField();
		sectionNameTextField.setBounds(173, 63, 130, 26);
		frame.getContentPane().add(sectionNameTextField);
		sectionNameTextField.setColumns(10);

		JButton createBtn = new JButton("Créer ");
		createBtn.addActionListener(e -> {
			String nom = sectionNameTextField.getText();
			RESULT res = SectionController.createSection(nom);
			switch (res) {
			case SUCCESS:
				JOptionPane.showMessageDialog(frame, "Ajouté avec succès", "succès",
						JOptionPane.INFORMATION_MESSAGE);
				break;
			case EMPTY_ERROR:
				JOptionPane.showMessageDialog(frame, "le nom du rayon ne peut pas être vide", "alert",
						JOptionPane.ERROR_MESSAGE);
				break;
			case DUPLICATE_ERROR:
				JOptionPane.showMessageDialog(frame, "Le nom du rayon existe déjà", "alert",
						JOptionPane.ERROR_MESSAGE);
				break;
			default:
				break;
			}

		});

		createBtn.setBounds(90, 206, 117, 29);
		frame.getContentPane().add(createBtn);

		JButton cancelBtn = new JButton("Annuler ");
		cancelBtn.addActionListener(e -> {
			frame.setVisible(false);
			managerMainWindow.getFrame().setVisible(true);
		});
		cancelBtn.setBounds(219, 206, 117, 29);
		frame.getContentPane().add(cancelBtn);
	}
}
