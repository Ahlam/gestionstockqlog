package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Section;
import model.User;

/**
 * <b> SectionDao is the class contains all data access objects related to
 * Section </b>
 * 
 * @author Yang YANG
 * @see Section
 */
public class SectionDao {
	/**
	 * EntityManagerFactory used to generate EntityManager
	 */
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");
	/**
	 * EntityManager used to access data
	 */
	private static EntityManager em = entityManagerFactory.createEntityManager();

	/**
	 * Remove a section from data server
	 * 
	 * @param id
	 * 
	 */
	public static void removeSection(int id) {
		em.getTransaction().begin();
		Section section = em.find(Section.class, id);
		em.remove(section);
		em.getTransaction().commit();
	}

	/**
	 * Create a section from data server
	 * 
	 * @param sectionName
	 * 
	 */
	public static void createSection(String sectionName) {
		em.getTransaction().begin();

		Section section = new Section();
		section.setSectionName(sectionName);
		em.persist(section);
		em.getTransaction().commit();
	}

	/**
	 * Get the section that has a name specific
	 * 
	 * @param sectionName
	 * 
	 */
	public static Section getSection(String sectionName) {
		em.getTransaction().begin();
		String hql = "From Section Where sectionName=?0 ";
		Query query = em.createQuery(hql);
		query.setParameter("0", sectionName);
		@SuppressWarnings("unchecked")
		List<Section> sections = query.getResultList();
		Section res = null;
		if (sections.size() == 1) {
			res = sections.get(0);
		}
		em.getTransaction().commit();
		return res;

	}

	/**
	 * Update section's attributes
	 * 
	 * @param id
	 * @param sectionName
	 * @param user
	 */
	public static void updateSection(int id, String sectionName, User user) {
		em.getTransaction().begin();
		Section section = em.find(Section.class, id);

		if (user != null) {
			User newU = em.find(User.class, user.getId());
			section.setSeller(newU);
			newU.setSection(section);
		} else {
			section.setSeller(user);
		}

		section.setSectionName(sectionName);

		em.getTransaction().commit();
	}

	/**
	 * Get all sections from data server
	 * 
	 */
	public static List<Section> getSectionList() {
		em.getTransaction().begin();
		em.clear();

		String hql = "From Section";
		Query query = em.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Section> sections = query.getResultList();

		em.getTransaction().commit();
		return sections;

	}
}
