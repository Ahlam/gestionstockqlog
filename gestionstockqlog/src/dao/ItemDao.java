package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Item;
import model.Section;

/**
 * <b> ItemDao is the class contains all data access objects related to Item
 * </b>
 * 
 * @author Yang YANG
 * @see Item
 */
public class ItemDao {
	/**
	 * EntityManagerFactory used to generate EntityManager
	 */
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");

	/**
	 * EntityManager used to access data
	 */
	private static EntityManager em = entityManagerFactory.createEntityManager();

	/**
	 * Update item's attributes
	 * 
	 * @param id
	 * @param itemName
	 * @param type
	 * 
	 */
	public static void updateItem(int id, String itemName, String type, int stock) {
		em.getTransaction().begin();
		Item article = em.find(Item.class, id);
		article.setitemName(itemName);
		article.setStock(stock);
		article.setType(type);
		em.getTransaction().commit();
	}

	/**
	 * Update item's attributes
	 * 
	 * @param id
	 * @param description
	 * 
	 */
	public static void updateItem(int id, String description) {
		em.getTransaction().begin();
		Item article = em.find(Item.class, id);
		article.setDescription(description);
		em.getTransaction().commit();
	}

	/**
	 * Update item's attributes
	 * 
	 * @param id
	 * @param stock
	 * 
	 */
	public static void updateItem(int id, int stock) {
		em.getTransaction().begin();
		Item article = em.find(Item.class, id);

		article.setStock(stock);

		em.getTransaction().commit();
	}

	/**
	 * Remove a item from data server
	 * 
	 * @param id
	 * 
	 */
	public static void removeItem(int id) {
		em.getTransaction().begin();
		Item article = em.find(Item.class, id);
		em.remove(article);
		em.getTransaction().commit();
	}

	/**
	 * Create a item
	 * 
	 * @param itemName
	 * @param type
	 * @param description
	 * @param stock
	 * @param sectionId
	 */
	public static void createItem(String itemName, String type, String description, int stock, int sectionId) {

		em.getTransaction().begin();
		Item a1 = new Item(itemName, type, description, stock);
		Section section = em.find(Section.class, sectionId);
		a1.setSection(section);
		em.persist(a1);

		em.getTransaction().commit();
	}

	/**
	 * Get all item that belongs to a specific section
	 * 
	 * @param sectionId
	 * 
	 */
	public static List<Item> getItemList(int sectionId) {
		em.getTransaction().begin();
		String hql = "From Item Where section_id=?0 ";
		Query query = em.createQuery(hql);
		query.setParameter(0, sectionId);
		@SuppressWarnings("unchecked")
		List<Item> articles = query.getResultList();

		em.getTransaction().commit();
		return articles;

	}

	/**
	 * Get the item that has a id specific
	 * 
	 * @param id
	 * 
	 */
	public static Item getItem(int id) {
		em.getTransaction().begin();
		Item article = em.find(Item.class, id);

		em.getTransaction().commit();
		return article;
	}
}
