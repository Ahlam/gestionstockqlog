package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import enums.FUNCTION;
import model.Section;
import model.User;
/**
 * <b> UserDao is the class contains all data access objects related to
 * User </b>
 * 
 * @author Yang YANG
 * @see User
 */
public class UserDao {
	/**
	 * EntityManagerFactory used to generate EntityManager
	 */
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");
	/**
	 * EntityManager used to access data
	 */
	private static EntityManager em = entityManagerFactory.createEntityManager();
	/**
	 * Verifier if a user is valid
	 * 
	 * @param id
	 * @param password
	 */
	public static boolean isValidUser(int id,String password) {
		boolean res = false;
		em.getTransaction().begin();
		User u = em.find(User.class,id);
		if(u!=null && u.getPassword().equals(password))
			res = true;
		em.getTransaction().commit();
		return res;
		
	}
	/**
	 * Get a user by id 
	 * @param id
	 */
	public static User getUser(int id) {
		em.getTransaction().begin();
		User u = em.find(User.class,id);
		
		em.getTransaction().commit();
		return u;
		
	}
	/**
	 * Get all users 
	 */
	public static List<User> getUserList( ) {
		em.getTransaction().begin();
		em.clear();
		String hql = "FROM User";
		Query query= em.createQuery(hql);
        @SuppressWarnings("unchecked")
		List<User> users =query.getResultList();
		
		em.getTransaction().commit(); 
		return users;
		
	}
	/**
	 * Get a user by name 
	 * @param firstName
	 * @param lastName
	 */
	public static User getUser(String firstName,String lastName) {
		em.getTransaction().begin();
		
		String hql = "FROM User Where firstName=?0 and lastName=?1 ";
		Query query= em.createQuery(hql);
		query.setParameter("0", firstName);
		query.setParameter("1", lastName);
        @SuppressWarnings("unchecked")
		List<User> users =query.getResultList();
        User res =null;
        if(users.size()==1) {
        	res= users.get(0);
        }
		em.getTransaction().commit();
		return res;
		
	}
	/**
	 * Remove a user by id 
	 * @param id
	 */
	public static void removeUser(int id) {
		em.getTransaction().begin();
		User user=em.find(User.class, id);
		em.remove(user);
		em.getTransaction().commit();
	}
	/**
	 * Create a user  
	 * @param lastName
	 * @param firstName
	 * @param section
	 * @param password
	 */
	public static void createUser(String lastName,String firstName,Section section,String password) {
		em.getTransaction().begin();
		User user=new User(lastName, firstName, FUNCTION.SELLER);
		if (section!=null) {
			Section sectionFind= em.find(Section.class, section.getId());
			user.setSection(sectionFind);
		}
		user.setPassword(password);
		em.persist(user);
		em.getTransaction().commit();
		
		
	}
	/**
	 * Update user's attributes
	 * 
	 * @param id
	 * @param lastName
	 * @param firstName
	 * @param section
	 * @param password
	 * 
	 */
	public static void updateUser(int id,String lastName,String firstName,Section section,String password) {
		em.getTransaction().begin();
		User user=em.find(User.class, id);
		Section sectionFind ;
		if(section !=null) {
			sectionFind= em.find(Section.class, section.getId());
		}else {
			sectionFind=null;
		}

		user.setLastName(lastName);
		user.setFirtsName(firstName);
		user.setPassword(password);
		user.setSection(sectionFind);
		em.getTransaction().commit();
		
	}
}
